package com.example.kiran_assignment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeRecyclerView
import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnItemDragListener
import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnItemSwipeListener
import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnListScrollListener
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    private lateinit var mAdapter: Adapter
    private lateinit var mList: DragDropSwipeRecyclerView
    private lateinit var lay: CoordinatorLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lay = findViewById(R.id.coordinatorLay)

        var dataSet = mutableListOf("18BCE0923 1")
        for (i in 2..15) {
            dataSet.add("18BCE0923 $i")
        }
        mAdapter = Adapter(dataSet)
        mList = findViewById(R.id.list)
        mList.layoutManager = LinearLayoutManager(this)
        mList.adapter = mAdapter
        mList.swipeListener = onItemSwipeListener
        mList.disableDragDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.DOWN)
        mList.disableDragDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.UP)
    }

    private val onItemSwipeListener = object : OnItemSwipeListener<String> {
        override fun onItemSwiped(position: Int, direction: OnItemSwipeListener.SwipeDirection, item: String): Boolean {
            if(direction.name == "RIGHT_TO_LEFT") {
                Snackbar.make(lay, "$item deleted", Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(lay, "$item archived", Snackbar.LENGTH_LONG).show();
            }
            return false
        }
    }
}