package com.example.kiran_assignment2

import android.view.View
import android.widget.TextView
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeAdapter

class Adapter(dataSet: List<String> = emptyList())
    : DragDropSwipeAdapter<String, Adapter.ViewHolder>(dataSet) {

    class ViewHolder(itemView: View) : DragDropSwipeAdapter.ViewHolder(itemView) {
        val itemText: TextView = itemView.findViewById(R.id.card_txt)
    }

    override fun getViewHolder(itemLayout: View) = Adapter.ViewHolder(itemLayout)

    override fun onBindViewHolder(item: String, viewHolder: Adapter.ViewHolder, position: Int) {
        // Here we update the contents of the view holder's views to reflect the item's data
        viewHolder.itemText.text = item
    }

    override fun getViewToTouchToStartDraggingItem(item: String, viewHolder: Adapter.ViewHolder, position: Int): View? {
        // We return the view holder's view on which the user has to touch to drag the item
        return viewHolder.itemText
    }
}